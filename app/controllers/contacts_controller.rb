class ContactsController < ApplicationController
    def new
        @contact = Contact.new
    end
    
    def create
        @contact = Contact.new(contact_params)
        
        if @contact.save
            
            name = params[:contact][:name]
            email = params[:contact][:email]
            body = params[:contact][:comments]
            ContactMailer.contact_email(name, email, body).deliver
            
            flash[:success] = "Your opinion was sent successfully"
            redirect_to root_path
        else
            flash[:danger] = "Please make sure you fill out all the details"
            render :new
        end
    end
    
    
    private
    
    def contact_params
        params.require(:contact).permit(:name, :email, :comments)
    end
end
