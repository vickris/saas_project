class ProfilesController < ApplicationController
    before_action :authenticate_user!
    before_action :only_current_user
    
    
    def new
        @user = User.find(params[:user_id])
        @profile = Profile.new
    end
    
    def create
        @user = User.find(params[:user_id])
        @profile = @user.build_profile(profile_params)
        if @profile.save
            flash[:success] = "Profile updated successfully"
            redirect_to user_path(params[:user_id])
        else
            render :new
        end
    end
    
    
    def edit 
        @user = User.find(params[:user_id])
        @profile = @user.profile
    end
    
    def update
        @user = User.find(params[:user_id])
        @profile = @user.profile
        
        if @profile.update(profile_params)
            flash[:success] = "Profile updated successfully"
            redirect_to user_path(params[:user_id])
        else
            render :edit
        end
    end
    
    private
    
    def profile_params
        params.require(:profile).permit(:first_name, :last_name, :avatar, :job_title, :contact_email, :phone_number, :description)
    end
    
    def only_current_user
        @user = User.find(params[:user_id])
        redirect_to root_path unless @user == current_user
    end
end
