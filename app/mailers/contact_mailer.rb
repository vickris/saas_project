class ContactMailer < ApplicationMailer::Base
    default to: "christophervundi@gmail.com"
    
    def contact_email(name, email, body)
        @name = name
        @email = email
        @body = body
        
        mail(from: email, subject: "From the contact us form")
    end
end